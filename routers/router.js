const {Router} = require('express');
const postController =require('../controllers/postController');
let router=Router();

router.get('/', postController.welcome);
router.post('/post/save', postController.savePost);

module.exports = router;
