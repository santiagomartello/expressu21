const { restart } = require('nodemon');
let Post = require('../Models/post');

function welcome(req, res){
    res.status(200).send(
    {
        message: 'welcome'
    }
    );

}


function savePost(req, res){
let myPost =new Post(req.body);
myPost.save((err, result)=>{
    if(err){
        res.status(500).send({message: "Post creation failed. Error 500"});

    }else{
        res.status(200).send({message: result});
    }
});
}

module.exports= {
    welcome,
    savePost
};
