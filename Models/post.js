
const mongoose= require('mongoose');

let Schema =mongoose.Schema;
const postSchema= Schema({
title: {type:String, required: true},
content: {type:String, required: true},
user: {type:String, required: true},
created: {type: Date, default: Date.now}
});

const Post= mongoose.model('posts', postSchema);

module.exports = Post;